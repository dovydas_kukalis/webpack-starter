import React from 'react';
import { Link } from 'react-router';
import { Header } from '../components/generic';

const AboutPage = () => (
  <>
    <Header title="About" />
    <Link to="/">Home</Link>
  </>
);

export default AboutPage;
