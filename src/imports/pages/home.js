import React from 'react';
import { Link } from 'react-router';
import { Header } from '../components/generic';

const HomePage = () => (
  <>
    <div className="beautiful">
      <Header title="Home" />
      <Link to="/about">About</Link>
    </div>
  </>
);

export default HomePage;
