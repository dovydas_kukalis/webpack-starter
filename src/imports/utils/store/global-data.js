import { initialState } from './initial-state';

export const globalData = (state = initialState, { type }) => {
  switch (type) {
    default:
      return state;
  }
};
