import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { browserHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';
import { initialState } from './initial-state';
import { rootReducer } from './root-reducer';

const middlewares = [thunk, routerMiddleware(browserHistory)];

export const store = createStore(
  rootReducer,
  initialState,
  compose(
    applyMiddleware(...middlewares),
    typeof window !== 'undefined' && window.devToolsExtension
      ? window.devToolsExtension()
      : f => f
  )
);
