import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { globalData } from './global-data';

export const rootReducer = combineReducers({
  globalData,
  routing: routerReducer
});
