import React from 'react';
import { render } from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { Provider } from 'react-redux';
import { store } from './utils/store';
import { HomePage, AboutPage } from './pages';

const history = syncHistoryWithStore(browserHistory, store);

render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={HomePage} />
      <Route path="about" component={AboutPage} />
    </Router>
  </Provider>,
  document.getElementById('app')
);
